@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <h2><b>ตัวเลือกการตั้งค่าผู้ใช้งาน</b></h2>
                <p class="text-danger">เรียนแจ้งผู้ใช้ระบบทุกท่าน : เพื่อการทำงานที่สมบูรณ์ กรุณาใช้ google chrome ในการใช้ระบบ</p>
            </br>
            <div class="container-fluid" style="width:90%;">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">

                        <a href="{{ route('adduser') }}">
                            <div class="small-box bg-blue card-shadow">
                                <div class="inner">
                                    <br>
                                    <p class="text-so-white text-center"><b>เพิ่มผู้ใช้งาน</b></p>
                                    <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class='fa fa-cubes' style='font-size:60px;'></i>
                                </div>
                                <a href="{{ route('adduser') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <a  href="{{ route('adjustuser') }}">
                            <div class="small-box bg-green card-shadow">
                                <div class="inner">
                                    <br>
                                    <p class="text-so-white text-center"><b>ตั้งค่าผู้ใช้งาน</b></p>
                                    <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-desktop" style='font-size:60px;'></i>
                                </div>
                                <a href="{{ route('adjustuser') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>
                    <!--
                    <div class="col-lg-3 col-xs-6">
                    <a href="/tagfg">
                    <div class="small-box bg-yellow card-shadow">
                    <div class="inner">
                    <br>
                    <p class="text-so-white text-center"><b>ระบบตัดสต้อกและออกของ</b></p>
                    <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                </div>
                <div class="icon">
                <i class="fas fa-dolly" style='font-size:60px;'></i>
            </div>
            <a href="/tagfg" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </a>
</div>

<div class="col-lg-3 col-xs-6">

    <a href="{{ route('decodeitem') }}">
        <div class="small-box bg-red card-shadow">
            <div class="inner">
                <br>
                <p class="text-so-white text-center"><b>ระบบตรวจสอบชนิดสินค้า</b></p>
                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('decodeitem') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </a>
</div>
<div class="col-lg-3 col-xs-6">

    <a href="{{ route('select') }}">
        <div class="small-box bg-aqua card-shadow">
            <div class="inner">
                <br>
                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </a>
</div>

<div class="col-lg-3 col-xs-6">

    <a href="{{ route('select') }}">
        <div class="small-box bg-green card-shadow">
            <div class="inner">
                <br>
                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </a>
</div>

<div class="col-lg-3 col-xs-6">

    <a href="{{ route('select') }}">
        <div class="small-box bg-yellow card-shadow">
            <div class="inner">
                <br>
                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </a>
</div>

<div class="col-lg-3 col-xs-6">

    <a href="{{ route('select') }}">
        <div class="small-box bg-red card-shadow">
            <div class="inner">
                <br>
                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class="icon">
                <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </a>
</div>
-->
</div>
</div>
</div>
</div>
</div>


@endsection
