@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive loading-page">
                    <table class="table table-striped table-bordered display" id="order">
                        <thead>
                            <tr class="text-table-so">
                                <th class="hidden-xs text-center">เลขที่ใบจอง</th>
                                <th class="text-center">เบอร์โทรศัพท์</th>
                                <th class="text-center">วันที่ส่ง</th>
                            </tr>
                        </thead>
                        <tbody>
                            {!! $the_view !!}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection
