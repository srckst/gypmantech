@extends('layouts.app')

@section('styles')

    @section('content')

        <div class="container-fluid bg-white">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="container-fluid">
                        <div class="card-header bg-white"><h4><b>ใบจองรหัส : {{ $showoder }}</b></h4>
                            <a href="{{ route('createse') }}" class="btn btn-info fa fa-file-text-o"><b>&nbsp;&nbsp;&nbsp;สร้างใบส่งของ</b></a>
                            <a href="{{ route('select') }}" class="text-white btn btn-warning fa fa-search"><b>&nbsp;&nbsp;&nbsp;กลับไปหน้าค้นหา</b></a>
                        </div>
                    </div>
                </br>
                <div class="container-fluid">
                    <p><b>รายการสินค้า</b></p>
                    <form class="form-inline md-form form-sm mt-0">
                        <input id="myInput" onkeyup="myFunction()" type="text" class="form-control" placeholder="รหัสสินค้า">
                        <button class="btn btn-primary btn-md" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>

                    <table id="myTable" class="table table-hover bg-white text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>รหัสใบจอง</th>
                                <th>รหัสสินค้า</th>
                                <th>รายการ</th>
                                <th>จำนวน</th>
                                <th>หน่วยนับ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($item as $items)
                                <tr>
                                    <td id='countTable'></td>
                                    <td>{{ $items->oder_number }}</td>
                                    <td>{{ $items->item_no }}</td>
                                    <td>{{ $items->item_list }}</td>
                                    <td>{{ $items->item_amount }}</td>
                                    <td>{{ $items->item_unit_of_measure }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>รหัสใบจอง</th>
                                <th>รหัสสินค้า</th>
                                <th>รายการ</th>
                                <th>จำนวน</th>
                                <th>หน่วยนับ</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        // Count row table
        var table = document.getElementsByTagName('table')[0],
        rows = table.getElementsByTagName('tr'),
        text = 'textContent' in document ? 'textContent' : 'innerText';

        for (var i = 1, len = rows.length; i < len-1; i++){
            rows[i].children[0][text] = i  + rows[i].children[0][text];
        }

    </script>

@endsection
