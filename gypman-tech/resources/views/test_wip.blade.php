@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <table id="myTableOut" class="table table-hover bg-white text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>รหัสซื้อขาย</th>
                            <th>จำนวน</th>
                            <th>ราคาต่อหน่วย</th>
                            <th>ตำแหน่งเก็บ</th>
                            <th>ลดต่อรายการ</th>
                            <th>เลขล็อต</th>
                        </tr>
                    </thead>
                    <tbody id="searchOut">
                        <tr>
                            <td id='countRow'></td>
                            <td class="t1" data-selected="true">B1</td>
                            <td>100</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td id='countRow'></td>
                            <td class="t2" data-selected="true">B1</td>
                            <td>100</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td id='countRow'></td>
                            <td class="t3" data-selected="true">B2</td>
                            <td>100</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td id='countRow'></td>
                            <td class="t4" data-selected="true">B2</td>
                            <td>100</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td id='countRow'></td>
                            <td class="t5" data-selected="true">B1</td>
                            <td>100</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="show1"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="show2"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="show3"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>รหัสซื้อขาย</th>
                            <th>จำนวน</th>
                            <th>ราคาต่อหน่วย</th>
                            <th>ตำแหน่งเก็บ</th>
                            <th>ลดต่อรายการ</th>
                            <th>เลขที่ล็อต</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>




@endsection
