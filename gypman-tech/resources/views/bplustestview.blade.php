@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="bplustest">
                            <thead>
                                <tr class="text-table-so">
                                    <th class="hidden-xs text-center">#</th>
                                    <th class="text-center">ใบจอง</th>
                                    <th class="text-center"><em class="fa fa-cog"></em></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
