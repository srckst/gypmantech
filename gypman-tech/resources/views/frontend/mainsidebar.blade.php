<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu</li>
            <!-- Optionally, you can add icons to the links -->

            <li><a href="{{ route('main') }}"><i class="glyphicon glyphicon-th"></i> <span>เมนูหลัก</span></a></li>
            <li><a href="#"><i class="fa fa-book"></i> <span>คู่มือการใช้งาน</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li>
            <li></br></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cogs"></i> <span>การตั้งค่า</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a class="fa fa-user-circle " href="{{ route('usersetting') }}">  การตั้งค่าผู้ใช้งาน</a></li>
                    <li><a class="fa fa-desktop" href="#">  การตั้งค่าเว็บไซต์</a></li>
                </ul>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>
