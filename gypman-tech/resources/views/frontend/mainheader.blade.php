<header class="main-header">

    <!-- Logo -->
    <a href="{{ route('main') }}" class="logo" style="background-color:#ae4522;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>G</b>MT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><!--<img type="image/png" src="{{ asset('img/Logo_GM_small.png') }}" alt="GMT" style="width:30%;">--><b>Gypman Tech</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation" style="background-color:#ed5f30;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>

                    @else

                        <a class="hidden-xs">{{ Auth::user()->name }} </a>
                        <li>
                            <a href="{{ route('logout') }}" class="" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Sign out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>
