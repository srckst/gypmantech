@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css">
@endsection

@section('content')
    <div class="panel-body">
        <table class="table table-striped table-bordered table-list">
            <thead>
                <tr>
                    <th class="text-center"><em class="fa fa-cog"></em></th>
                    <th class="hidden-xs text-center">ID</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Date</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($se_show as $se_show)
                <tr>
                <td align="center">
                <a class="btn btn-outline-danger"><em class="fa fa-trash"></em></a>
            </td>
            <td class="hidden-xs text-center">{{ $se_show->se_id }}</td>
            <td class="text-center">{{ $se_show->se_name }}</td>
            <td class="text-center">{{ $se_show->se_created_at }}</td>
        </tr>
        @endforeach

</tbody>
</table>
</div>



@endsection

@section('javascripts')

    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js"></script>



    <script>

        function goBack() {
            window.location.href = "{{ route('select') }}";
        }

        $(document).ready(function(){
            $("#myBtn").click(function(){
                $("#myModal").modal();
            });
        });

        $(document).ready( function () {
            $('#setable').DataTable();
        });


    </script>


@endsection
