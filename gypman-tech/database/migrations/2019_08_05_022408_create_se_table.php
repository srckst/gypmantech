<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('se_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('se_oder_number',11);
            $table->string('se_pleace');
            $table->string('se_tell');
            $table->date('se_sended_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('se_table');
    }
}
