<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemSe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_se', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('se_id');
            $table->string('oder_number',11);
            $table->string('item_no',11);
            $table->string('item_list');
            $table->integer('item_amount');
            $table->string('item_unit_of_measure');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_se');
    }
}
