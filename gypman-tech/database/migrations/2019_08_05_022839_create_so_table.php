<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('so_oder_number',11);
            $table->string('so_pleace');
            $table->string('so_tell');
            $table->date('so_sended_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_table');
    }
}
