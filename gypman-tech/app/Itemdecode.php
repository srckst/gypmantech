<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itemdecode extends Model
{
    protected $table = "item_decode";
    protected $fillable = ['item_code'];
}
