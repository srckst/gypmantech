<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wipline1 extends Model
{
    protected $table = 'wipline1';

    protected $fillable = ['id','wl1_barcode','wl1_brand','wl1_amount'];
}
