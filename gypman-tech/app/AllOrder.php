<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllOrder extends Model
{
    protected $table = 'DOCINFO';

    protected $fillable = ['DI_REF'];
}
