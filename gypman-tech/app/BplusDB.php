<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BplusDB extends Model
{
    protected $connection = 'sqlsrv_bplus';
}
