<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wipbarcode extends Model
{
    protected $table = "wipbarcode";

    protected $fillable = ['wip_barcode'];
}
