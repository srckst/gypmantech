<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wipline2 extends Model
{
    protected $table = 'wipline2';

    protected $fillable = ['wl2_barcode'];
}
