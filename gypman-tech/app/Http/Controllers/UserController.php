<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function index(){
        $view = view('userfunction');
        return $view;
    }

    public function adduser(){
        $view = view('adduser');
        return $view;
    }

    public function adjustuser(){
        $count = 1;
        $data = User::all();
        $view = view('adjustuser',compact('data','count'));
        return $view;
    }
}
