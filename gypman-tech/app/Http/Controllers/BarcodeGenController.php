<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Picqer;

class BarcodeGenController extends Controller
{
    public function genwipline1a(Request $request){

        $validator = Validator::make($request->all(), [
            'barcode' => 'required|string'
        ]);

        if (!$validator->fails()) {
            $label = $request->input('barcode');

            $barcode_generator = new Picqer\Barcode\BarcodeGeneratorJPG();
            $barcode = $barcode_generator->getBarcode($label, $barcode_generator::TYPE_CODE_128);

            $view = view('wip_line1a',['label' => $label, 'barcode' => $barcode]);
            return $view;
        }

        return response()->json($validator->messages(), 400 , array(), JSON_PRETTY_PRINT);
    }
}
