// Wip barcode condition

$('.codewip3').each(function(){
    var newText = ($(this).text()).substr(0,11);
    if (newText == 'W199-A10109') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ธรรมดา 1.2 x 2.4 m. x9 mm.');
    }
    else if (newText == 'W199-A10112') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ธรรมดา 1.2 x 2.4 m. x12 mm.');
    }
    else if (newText == 'W199-A10209') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ธรรมดา 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W199-A10212') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ธรรมดา 1.22 x 2.44 m. x12 mm.');
    }
    else if (newText == 'W199-A10595') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ธรรมดา 1.2 x 3 m. x9.5 mm');
    }
    else if (newText == 'W199-A30109') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ทนชื้น 1.2 x 2.4 m. x9 mm.');
    }
    else if (newText == 'W199-A30615') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด ทนชื้น 1.2 x 2.5 m. x12.5mm.');
    }
    else if (newText == 'W199-B10109') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ธรรมดา 1.2 x 2.4 m. x9 mm.');
    }
    else if (newText == 'W199-B10112') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ธรรมดา 1.2 x 2.4 m. x12 mm.');
    }
    else if (newText == 'W199-B10185') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ธรรมดา 1.2 x 2.4 m. x8.5 mm');
    }
    else if (newText == 'W199-B10209') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ธรรมดา 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W199-B10309') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ธรรมดา 1.21 x 2.42 cm x9 mm.');
    }
    else if (newText == 'W199-B10412') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ธรรมดา 1.22 x 3.05 m. x12 mm.');
    }
    else if (newText == 'W199-B30209') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ทนชื้น 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W199-B30309') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ทนชื้น 1.21 x 2.42 cm x9 mm.');
    }
    else if (newText == 'W199-B60209') {
        $(this).text('แผ่นรอคัด Line 1 ขอบเรียบ ทนไฟ 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W299-A10109') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ธรรมดา 1.2 x 2.4 m. x9 mm.');
    }
    else if (newText == 'W299-A10112') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ธรรมดา 1.2 x 2.4 m. x12 mm.');
    }
    else if (newText == 'W299-A10209') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ธรรมดา 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W299-A10212') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ธรรมดา 1.22 x 2.44 m. x12 mm.');
    }
    else if (newText == 'W299-A10595') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ธรรมดา 1.2 x 3 m. x9.5 mm');
    }
    else if (newText == 'W299-A30109') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ทนชื้น 1.2 x 2.4 m. x9 mm.');
    }
    else if (newText == 'W299-A30615') {
        $(this).text('แผ่นรอคัด Line 2 ขอบลาด ทนชื้น 1.2 x 2.5 m. x12.5mm.');
    }
    else if (newText == 'W299-B10109') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ธรรมดา 1.2 x 2.4 m. x9 mm.');
    }
    else if (newText == 'W299-B10112') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ธรรมดา 1.2 x 2.4 m. x12 mm.');
    }
    else if (newText == 'W299-B10185') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ธรรมดา 1.2 x 2.4 m. x8.5 mm');
    }
    else if (newText == 'W299-B10209') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ธรรมดา 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W299-B10309') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ธรรมดา 1.21 x 2.42 cm x9 mm.');
    }
    else if (newText == 'W299-B10412') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ธรรมดา 1.22 x 3.05 m. x12 mm.');
    }
    else if (newText == 'W299-B30209') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ทนชื้น 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W299-B30309') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ทนชื้น 1.21 x 2.42 cm x9 mm.');
    }
    else if (newText == 'W299-B60209') {
        $(this).text('แผ่นรอคัด Line 2 ขอบเรียบ ทนไฟ 1.22 x 2.44 m. x9 mm.');
    }
    else if (newText == 'W199-A40109') {
        $(this).text('แผ่นรอคัด Line 1 ขอบลาด กันร้อน 1.2 x 2.4 m. x9 mm.');
    }
    else {
        $(this).text('ไม่พบข้อมูลชนิดสินค้า');
    }
});
